# Potter books kata using Javascript and React

## Problem Description
Once upon a time there was a series of 5 books about a very famous English hero called Harry.
(At least when this Kata was invented, there were only 5. Since then they have multiplied)
Children all over the world thought he was fantastic, and, of course, so did the publisher.
So in a gesture of immense generosity to mankind, (and to increase sales) they set up the following
pricing model to take advantage of Harry’s magical powers.

One copy of any of the five books costs 8 EUR. If, however, you buy two different books from the series,
you get a 5% discount on those two books. If you buy 3 different books, you get a 10% discount.
With 4 different books, you get a 20% discount. If you go the whole hog, and buy all 5, you get a huge
25% discount.

Note that if you buy, say, four books, of which 3 are different titles, you get a 10% discount on the 3
that form part of a set, but the fourth book still costs 8 EUR.

Potter mania is sweeping the country and parents of teenagers everywhere are queueing up with shopping
baskets overflowing with Potter books. Your mission is to write a piece of code to calculate the price
of any conceivable shopping basket, giving as big a discount as possible.

For example, how much does this basket of books cost?

* 2 copies of the first book
* 2 copies of the second book
* 2 copies of the third book
* 1 copy of the fourth book
* 1 copy of the fifth book
answer :
```
(4 * 8) - 20% [first book, second book, third book, fourth book]
+ (4 * 8) - 20% [first book, second book, third book, fifth book]
  = 25.6 * 2
  = 51.20
```

## How to run this project
This is a vue project using Typescript and Vite.
For running the application, you need to download all dependencies:
```
$ npm install
```
Now you can run the application using:
```
$ npm run start
  vite v2.4.1 dev server running at:

  > Local: http://localhost:3000/
  > Network: use `--host` to expose

  ready in 252ms.
```
You can navigate to [http://localhost:3000/](http://localhost:3000/) to
see the result.

## Clues
You’ll find that this Kata is easy at the start. You can get going with tests for baskets of 0 books,
1 book, 2 identical books, 2 different books… and it is not too difficult to work in small steps and
gradually introduce complexity.

However, the twist becomes apparent when you sit down and work out how much you think the sample basket
above should cost. It isn’t `5 * 8 * 0.75 + 3 * 8 * 0.90`. It is in fact `4 * 8 * 0.8 + 4 * 8 * 0.8`.
So the trick with this Kata is not that the acceptance test you’ve been given is wrong.
The trick is that you have to write some code that is intelligent enough to notice that two sets of four
books is cheaper than a set of five and a set of three.

You will have to introduce a certain amount of clever optimization algorithm. But not too much!
This problem does not require a fully fledged general purpose optimizer. Try to solve just this
problem well in order to share it for everyone or even in the ??? . Trust that you can generalize
and improve your solution if and when new requirements come along.

## React + Javascript + Craco + Tailwind css

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

As we use Tailwind css we setup the project using [this documentation](https://tailwindcss.com/docs/guides/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm run start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

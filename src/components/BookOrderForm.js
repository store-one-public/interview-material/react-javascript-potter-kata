function BookOrderForm() {
    return (
        <div className="h-screen w-full flex flex-wrap justify-center items-center">
            <div className="w-full md:w-1/2">
                <div className="bg-white">
                    <div className="flex bg-theme-blue-400 text-white bg-gray-800 px-4">
                        <div className="w-full py-2 text-xl md:text-3xl font-bold text-center">
                            Potter books order
                        </div>
                    </div>
                    <div className="py-4 px-2 overflow-y-scroll bg-gray-700 text-white">
                        <div className="flex justify-center">
                            <form id="orderBookForm" className="md:px-4 w-full">
                                <div className="flex mb-4 w-full bg-white rounded-lg p-2">
                                    <label
                                        for="first_book_input"
                                        className="w-full flex pl-6 self-center text-black font-semibold"
                                    >
                                        <span className="self-center text-base md:text-3xl">
                                            Harry Potter and the Philosopher's Stone
                                        </span>
                                    </label>
                                    <div className="inline-block relative">
                                        <div className="flex flex-wrap">
                                            <div className="flex w-8/12">
                                                <input type="text" value="0" id="first_book_input"
                                                    className="bg-white text-sm text-gray-900 text-center focus:outline-none border border-gray-800 focus:border-gray-600 rounded-l-md w-full" />
                                            </div>
                                            <div className="flex flex-col w-4/12">
                                                <button
                                                    className="text-white text-center text-md font-semibold rounded-tr-md px-1 bg-gray-800 focus:bg-gray-600 focus:outline-none border border-gray-800 focus:border-gray-600">
                                                    +
                                                </button>
                                                <button
                                                    className="text-white text-center text-md font-semibold rounded-br-md px-1 bg-gray-800 focus:bg-gray-600 focus:outline-none border border-gray-800 focus:border-gray-600">
                                                    -
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="flex mb-4 w-full bg-white rounded-lg p-2">
                                    <label
                                        for="first_book_input"
                                        className="w-full flex pl-6 self-center text-black font-semibold"
                                    >
                                        <span className="self-center text-base md:text-3xl">
                                            Harry Potter and the Chamber of Secrets
                                        </span>
                                    </label>
                                    <div className="inline-block relative">
                                        <div className="flex flex-wrap">
                                            <div className="flex w-8/12">
                                                <input type="text" value="0" id="first_book_input"
                                                    className="bg-white text-sm text-gray-900 text-center focus:outline-none border border-gray-800 focus:border-gray-600 rounded-l-md w-full" />
                                            </div>
                                            <div className="flex flex-col w-4/12">
                                                <button
                                                    className="text-white text-center text-md font-semibold rounded-tr-md px-1 bg-gray-800 focus:bg-gray-600 focus:outline-none border border-gray-800 focus:border-gray-600">
                                                    +
                                                </button>
                                                <button
                                                    className="text-white text-center text-md font-semibold rounded-br-md px-1 bg-gray-800 focus:bg-gray-600 focus:outline-none border border-gray-800 focus:border-gray-600">
                                                    -
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="flex mb-4 w-full bg-white rounded-lg p-2">
                                    <label
                                        for="first_book_input"
                                        className="w-full flex pl-6 self-center text-black font-semibold"
                                    >
                                        <span className="self-center text-base md:text-3xl">
                                            Harry Potter and the Prisoner of Azkaban
                                        </span>
                                    </label>
                                    <div className="inline-block relative">
                                        <div className="flex flex-wrap">
                                            <div className="flex w-8/12">
                                                <input type="text" value="0" id="first_book_input"
                                                    className="bg-white text-sm text-gray-900 text-center focus:outline-none border border-gray-800 focus:border-gray-600 rounded-l-md w-full" />
                                            </div>
                                            <div className="flex flex-col w-4/12">
                                                <button
                                                    className="text-white text-center text-md font-semibold rounded-tr-md px-1 bg-gray-800 focus:bg-gray-600 focus:outline-none border border-gray-800 focus:border-gray-600">
                                                    +
                                                </button>
                                                <button
                                                    className="text-white text-center text-md font-semibold rounded-br-md px-1 bg-gray-800 focus:bg-gray-600 focus:outline-none border border-gray-800 focus:border-gray-600">
                                                    -
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="flex mb-4 w-full bg-white rounded-lg p-2">
                                    <label
                                        for="first_book_input"
                                        className="w-full flex pl-6 self-center text-black font-semibold"
                                    >
                                        <span className="self-center text-base md:text-3xl">
                                            Harry Potter and the Goblet of Fire
                                        </span>
                                    </label>
                                    <div className="inline-block relative">
                                        <div className="flex flex-wrap">
                                            <div className="flex w-8/12">
                                                <input type="text" value="0" id="first_book_input"
                                                    className="bg-white text-sm text-gray-900 text-center focus:outline-none border border-gray-800 focus:border-gray-600 rounded-l-md w-full" />
                                            </div>
                                            <div className="flex flex-col w-4/12">
                                                <button
                                                    className="text-white text-center text-md font-semibold rounded-tr-md px-1 bg-gray-800 focus:bg-gray-600 focus:outline-none border border-gray-800 focus:border-gray-600">
                                                    +
                                                </button>
                                                <button
                                                    className="text-white text-center text-md font-semibold rounded-br-md px-1 bg-gray-800 focus:bg-gray-600 focus:outline-none border border-gray-800 focus:border-gray-600">
                                                    -
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="flex mb-4 w-full bg-white rounded-lg p-2">
                                    <label
                                        for="first_book_input"
                                        className="w-full flex pl-6 self-center text-black font-semibold"
                                    >
                                        <span className="self-center text-base md:text-3xl">
                                            Harry Potter and the Order of the Phoenix
                                        </span>
                                    </label>
                                    <div className="inline-block relative">
                                        <div className="flex flex-wrap">
                                            <div className="flex w-8/12">
                                                <input type="text" value="0" id="first_book_input"
                                                    className="bg-white text-sm text-gray-900 text-center focus:outline-none border border-gray-800 focus:border-gray-600 rounded-l-md w-full" />
                                            </div>
                                            <div className="flex flex-col w-4/12">
                                                <button
                                                    className="text-white text-center text-md font-semibold rounded-tr-md px-1 bg-gray-800 focus:bg-gray-600 focus:outline-none border border-gray-800 focus:border-gray-600">
                                                    +
                                                </button>
                                                <button
                                                    className="text-white text-center text-md font-semibold rounded-br-md px-1 bg-gray-800 focus:bg-gray-600 focus:outline-none border border-gray-800 focus:border-gray-600">
                                                    -
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="text-center pt-8">
                                    <button
                                        type="submit"
                                        className="px-4 py-2 text-xl md:text-3xl rounded-lg bg-blue-400 text-white "
                                    >
                                        Get price
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default BookOrderForm;

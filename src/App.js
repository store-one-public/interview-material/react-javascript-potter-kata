import logo from './logo.svg';
import './App.css';
import BookOrderForm from './components/BookOrderForm'

function App() {
  return (
    <div className="App">
      <BookOrderForm/>
    </div>
  );
}

export default App;
